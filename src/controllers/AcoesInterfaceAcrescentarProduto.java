package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import model.Bebida;
import model.Comida;
import model.Funcionario;
import model.Sobremesa;
import view.Pedido;

public class AcoesInterfaceAcrescentarProduto implements ActionListener {
	
	private JFrame telaInicial;
	private JPanel painelAcrescentarProduto;
	private JTextField produtoEntrada;
	private JTextField quantidadeEntrada;
	private JTextField precoEntrada;
	private JRadioButton comida;
	private JRadioButton bebida;
	private JRadioButton sobremesa;

	public AcoesInterfaceAcrescentarProduto(JPanel painelAcrescentarProduto,JFrame telaInicial,JTextField produtoEntrada
			,JTextField quantidadeEntrada,JTextField precoEntrada,JRadioButton comida,JRadioButton bebida,JRadioButton sobremesa
			) throws IOException{
		this.painelAcrescentarProduto=painelAcrescentarProduto;
		this.telaInicial=telaInicial;
		this.produtoEntrada=produtoEntrada;
		this.quantidadeEntrada=quantidadeEntrada;
		this.precoEntrada=precoEntrada;
		this.comida=comida;
		this.bebida=bebida;
		this.sobremesa=sobremesa;
		
	}
	
	
	public void salvarArquivo2(String produto,String quantidade,String preco) throws IOException{
		PrintWriter gravarArq = new PrintWriter(new FileWriter("/home/joaorobson/Documentos/"
				+ "/produtos.txt",true));
		 
			
			gravarArq.println(produto );
			gravarArq.println(quantidade);
			gravarArq.println(preco);
			gravarArq.close();
		
	    	 
	    
	  
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		
		String comando = e.getActionCommand();
		
		if(comando.equals("Ok")){
			
			if(quantidadeEntrada.getText().equals("")||produtoEntrada.getText().equals("")||precoEntrada.getText().equals("")){
				JOptionPane.showMessageDialog(null,"Algum campo está em branco!");
				
				
			}
			else if(quantidadeEntrada.getText().matches("[0-9]+")==false||precoEntrada.getText().matches("[0-9]+")==false){
				JOptionPane.showMessageDialog(null,"Alguma letra foi digitada no campo \"Insira a quantidade do produto\" "
						+ "ou no campo \"Insira o preço do produto\" ! ");
			}
			else{
				try {
					salvarArquivo2(produtoEntrada.getText(),quantidadeEntrada.getText(),precoEntrada.getText());
					Funcionario[] novo = new Funcionario[]{new Funcionario(comando, comando, comando)};
					
				} catch (IOException e1) {
					System.out.println("Arquivo não encontrado!");
				}
				JOptionPane.showMessageDialog(null,"Cadastro realizado com sucesso!");
				if(comida.isSelected()==true){
					List<Comida> pratos = new ArrayList<Comida>();
					pratos.add(new Comida(produtoEntrada.getText()
							,quantidadeEntrada.getText(),precoEntrada.getText()));
				}
				else if(bebida.isSelected()==true){
					List<Bebida> bebidas = new ArrayList<Bebida>();
					bebidas.add(new Bebida(produtoEntrada.getText()
							,quantidadeEntrada.getText(),precoEntrada.getText()));
				}
				else if(sobremesa.isSelected()==true){
					
					List<Sobremesa> sobremesas = new ArrayList<Sobremesa>();
					sobremesas.add(new Sobremesa(produtoEntrada.getText()
							,quantidadeEntrada.getText(),precoEntrada.getText()));
					painelAcrescentarProduto.setVisible(false);
				}
				new Pedido(telaInicial);
			
			}
		    
			
		}
		
		else if(comando.equals("Cancelar")){
			produtoEntrada.setText("");
			quantidadeEntrada.setText("");
			precoEntrada.setText("");
			
			
		}
				
		else {
			painelAcrescentarProduto.setVisible(false);
			new Pedido(telaInicial);
			
		}
		

	}

	
	
}
