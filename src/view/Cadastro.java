package view;

import java.awt.Font;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controllers.AcoesInterfaceCadastro;

public class Cadastro {
	private JPanel painelCadastro;
	
	public Cadastro(JFrame telaInicial) throws IOException{
		
		criaPainelCadastros(telaInicial);
	}

	private void criaPainelCadastros(JFrame telaInicial) throws IOException {
		
		JLabel nome = new JLabel("Insira seu nome");
		JTextField nomeEntrada = new JTextField();
		JLabel login = new JLabel("Insira seu login");
		JTextField loginEntrada = new JTextField();
		JLabel senha = new JLabel("Insira sua senha");
		JPasswordField senhaEntrada = new JPasswordField();
		JLabel titulo = new JLabel("Cadastro");
		JButton ok = new JButton("Ok");
		JButton cancelar = new JButton("Cancelar");
		JButton voltar = new JButton("Voltar");
		
		titulo.setBounds(450,100,200,40);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		ok.setBounds(345,330,150,30);
		cancelar.setBounds(505,330,150,30);
		voltar.setBounds(345,370, 310, 30);
		
		nome.setBounds(345,150,200,20);
		nomeEntrada.setBounds(345, 180, 310, 20);
		
		login.setBounds(345,210,200,20);
		loginEntrada.setBounds(345, 240, 310, 20);
		
		
		senha.setBounds(345,270,200,20);
		senhaEntrada.setBounds(345,300, 310, 20);
		
		
		
		painelCadastro = new JPanel();
		painelCadastro.setLayout(null);
		
		ok.setActionCommand("Ok");
		cancelar.setActionCommand("Cancelar");
		
		ok.addActionListener(new AcoesInterfaceCadastro(painelCadastro,telaInicial,nomeEntrada,loginEntrada,senhaEntrada));
		cancelar.addActionListener(new AcoesInterfaceCadastro(painelCadastro,telaInicial,nomeEntrada,loginEntrada,senhaEntrada));
		voltar.addActionListener(new AcoesInterfaceCadastro(painelCadastro,telaInicial,nomeEntrada,loginEntrada,senhaEntrada));
		
		painelCadastro.add(ok);
		painelCadastro.add(cancelar);
		painelCadastro.add(voltar);
		painelCadastro.add(nome);
		painelCadastro.add(nomeEntrada);
		painelCadastro.add(login);
		painelCadastro.add(loginEntrada);
		painelCadastro.add(senha);
		painelCadastro.add(senhaEntrada);
		
		telaInicial.add(painelCadastro);
		telaInicial.setVisible(true);
		painelCadastro.setVisible(true);
	}
	
	
}

