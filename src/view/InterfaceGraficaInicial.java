package view;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controllers.AcoesInterfaceInicial;





public class InterfaceGraficaInicial  {
	
	
	
	private JPanel painelInicial;
	private JLabel painelImagem;
	
	public InterfaceGraficaInicial(JFrame telaInicial){
		
		
		criaPainelInicial(telaInicial);
		
		
		
	}
	
	
	
	public void criaPainelInicial(JFrame telaInicial){
		
		
		
		painelInicial = new JPanel();
		painelInicial.setLayout(null);
		painelInicial.setLocation((telaInicial.getWidth()-painelInicial.getWidth())/2,(telaInicial.getHeight()-painelInicial.getHeight())/2);
		
		
		JButton ok = new JButton("Ok");
		JButton cadastrar = new JButton("Não possui cadastro?");
		JButton cancelar = new JButton("Cancelar");
		
		JLabel titulo = new JLabel("Login", SwingConstants.CENTER);
		JLabel login = new JLabel("Insira seu login");
		JTextField loginEntrada = new JTextField();
		
		JLabel senha = new JLabel("Insira sua senha");
		JPasswordField senhaEntrada = new JPasswordField();
		
		
		titulo.setBounds(400,100,200,40);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		ok.setBounds(345,300,150,30);
		cadastrar.setBounds(345,340,310,30);
		cancelar.setBounds(505,300,150,30);
		
		
		login.setBounds(345, 150, 200, 20);
		loginEntrada.setBounds(345,180,310 , 20);
		
		
		senha.setBounds(345, 210, 200, 20);
		senhaEntrada.setBounds(345,250,310 , 20);
		
		
	    
		
		
		
		painelImagem = new JLabel();
		painelImagem.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		painelImagem.setBounds(0, 0, 1000,100);
		painelImagem.setIcon(new ImageIcon("/home/joaorobson/Imagens/pizza.jpg"));
		telaInicial.add(painelImagem);
		telaInicial.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		
		
		ok.setActionCommand("Ok");
		cadastrar.setActionCommand("Cadastrar");
		cancelar.setActionCommand("Cancelar");
		ok.addActionListener(new AcoesInterfaceInicial(painelInicial,telaInicial,loginEntrada,senhaEntrada));
		cadastrar.addActionListener(new AcoesInterfaceInicial(painelInicial,telaInicial,loginEntrada,senhaEntrada));
		cancelar.addActionListener(new AcoesInterfaceInicial(painelInicial,telaInicial,loginEntrada,senhaEntrada));
		telaInicial.add(painelInicial);
		painelInicial.add(titulo);
		painelInicial.add(ok);
		painelInicial.add(cadastrar);
		painelInicial.add(cancelar);
		painelInicial.add(login);
		painelInicial.add(loginEntrada);
		painelInicial.add(senha);
		painelInicial.add(senhaEntrada);
		
		

		telaInicial.setVisible(true);
		//painelInicial.setVisible(true);
		//painelImagem.setVisible(true);
		}
		
		
	}
	


