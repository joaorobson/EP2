package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import model.Funcionario;
import model.Sobremesa;
import view.InterfaceGraficaInicial;

public class AcoesInterfaceCadastro implements ActionListener {

	private JPanel painelCadastro;
	private JFrame telaInicial;
	private JTextField nomeEntrada;
	private JTextField loginEntrada;
	private JPasswordField senhaEntrada;
	
	public AcoesInterfaceCadastro(JPanel painelCadastro,JFrame telaInicial,JTextField nomeEntrada,JTextField loginEntrada,JPasswordField senhaEntrada) throws IOException{
		this.painelCadastro=painelCadastro;
		this.telaInicial=telaInicial;
		this.nomeEntrada=nomeEntrada;
		this.loginEntrada=loginEntrada;
		this.senhaEntrada=senhaEntrada;
		
		
	}
	
	
	public void salvarArquivo(String nome,String login,String senha) throws IOException{
		PrintWriter gravarArq = new PrintWriter(new FileWriter("/home/joaorobson/Documentos/logins.txt",true));
		 
			
			gravarArq.println(nome);
			gravarArq.println(login);
			gravarArq.println(senha);
			
			gravarArq.close();
		
	    	 
	    
	  
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		
		
		String comando = e.getActionCommand();
		
		if(comando.equals("Ok")){
			
			if(loginEntrada.getText().equals("")||senhaEntrada.getText().equals("")||nomeEntrada.getText().equals("")){
				JOptionPane.showMessageDialog(null,"Algum campo está em branco!");
				
				
			}
			else{
				try {
					
					salvarArquivo(nomeEntrada.getText(),loginEntrada.getText(),senhaEntrada.getText());
					List<Funcionario> funcionarios = new ArrayList<Funcionario>();
					funcionarios.add(new Funcionario(nomeEntrada.getText(),loginEntrada.getText(),senhaEntrada.getText()));
					
				} catch (IOException e1) {
					System.out.println("Arquivo não encontrado!");
				}
				JOptionPane.showMessageDialog(null,"Cadastro realizado com sucesso!");
				painelCadastro.setVisible(false);
				new InterfaceGraficaInicial(telaInicial);
			}
		    
			
		}
		else if(comando.equals("Cancelar")){
			nomeEntrada.setText("");
			loginEntrada.setText("");
			senhaEntrada.setText("");
			
			
		}
		else{
			painelCadastro.setVisible(false);
			new InterfaceGraficaInicial(telaInicial);
			
		}
	}

	
	
}
