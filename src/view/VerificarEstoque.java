package view;

import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controllers.AcoesInterfaceAcrescentarProduto;
import controllers.AcoesInterfaceVerificarEstoque;

public class VerificarEstoque {
	private JPanel painelVerificarEstoque;
	
	public VerificarEstoque(JFrame telaInicial) throws IOException {
			
			criaPainelVerificarEstoque(telaInicial);
		}

	private void criaPainelVerificarEstoque(JFrame telaInicial) throws IOException  {
		
		JButton voltar=new JButton ("Voltar");
		JLabel titulo = new JLabel("Verificar estoque",SwingConstants.CENTER);
		JLabel produtos = new JLabel("Produtos");
		JLabel quantidade = new JLabel("Quantidade");
		
		painelVerificarEstoque = new JPanel();
		painelVerificarEstoque.setLayout(null);
		
		
		voltar.setBounds(400,450, 200, 30);
		titulo.setBounds(400,100,200,100);
		quantidade.setBounds(800,110,200,100);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		produtos.setBounds(200, 120, 200, 100);
	
		
		voltar.setActionCommand("Voltar");
		voltar.addActionListener(new AcoesInterfaceVerificarEstoque( telaInicial, painelVerificarEstoque));
	
		
		painelVerificarEstoque.add(lerProdutos());
		painelVerificarEstoque.add(titulo);
		
		painelVerificarEstoque.add(voltar);
		telaInicial.add(painelVerificarEstoque);
		
		telaInicial.setVisible(true);
		
		
		
	}

	
	private JScrollPane lerProdutos() throws IOException {
		@SuppressWarnings("resource")
		BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/produtos.txt"));
		String estoque = "";
		System.getProperty("line.separator");
		DefaultListModel<String> modelLista = new DefaultListModel<String>();
		modelLista.setSize(30);

		JList lista = new JList(modelLista);
		lista.setVisibleRowCount(-1);
		JScrollPane listScroller = new JScrollPane(lista);
		listScroller.setPreferredSize(new Dimension(20,20));
		listScroller.setBounds(300, 200,400, 150);
		
		while( leitor.ready() ){
			String produto=leitor.readLine();
			String quantidade=leitor.readLine();
			leitor.readLine();
			
			
			
			
			estoque = "Produto: " + produto +"---------"+ "Quantidade: " +quantidade;
			modelLista.addElement(estoque);
		}	
		

		
		
		
		
	
		return listScroller;
	}
	
	}
	
