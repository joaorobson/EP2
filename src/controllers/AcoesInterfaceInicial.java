package controllers;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import view.Pedido;
import view.Cadastro;
import view.InterfaceGraficaInicial;





public class AcoesInterfaceInicial  implements ActionListener{
		
	private JPanel painelInicial;
	private JFrame telaInicial;
	private JTextField loginEntrada;
	private JPasswordField senhaEntrada;
	
	

	public AcoesInterfaceInicial(JPanel painelInicial, JFrame telaInicial, 
			JTextField loginEntrada, JPasswordField senhaEntrada) {
		this.painelInicial=painelInicial;
		this.telaInicial=telaInicial;
		this.loginEntrada=loginEntrada;
		this.senhaEntrada=senhaEntrada;
	}

	public boolean lerArquivo(String login, String senha) throws IOException{
		
		
		BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/logins.txt"));
				
		while( leitor.ready() ){
			String nome=leitor.readLine() ;
			String loginLido= leitor.readLine();
			String senhaLida=leitor.readLine();
			
			
			
			if(senhaLida.equals(senha)&&loginLido.equals(login)){
				
				painelInicial.setVisible(false);
				Pedido pedir = new Pedido(telaInicial);
				return true;
			}
			
			}
		return false;
		
			
		
	}
	
	

	public void actionPerformed(ActionEvent e){
			
		
		String comando = e.getActionCommand();
		
		if(comando.equals("Ok")){
			
			if(loginEntrada.getText().equals("")||senhaEntrada.getText().equals("")){
				JOptionPane.showMessageDialog(null,"Algum campo está em branco!","Aviso",1);
				
				
			}
			else{
			try {
				if(lerArquivo(loginEntrada.getText(),senhaEntrada.getText())==false){
					JOptionPane.showMessageDialog(null,"Login e/ou senha errados","Aviso",0);
				}
			} catch (IOException e1) {
				System.out.println("Nope");
			}
			
			
			}
			
			
		}
		else if(comando.equals("Cancelar")){
			loginEntrada.setText("");
			senhaEntrada.setText("");
			
			
		}
		else{
			painelInicial.setVisible(false);
			try {
				Cadastro cadastro = new Cadastro(telaInicial);
			} catch (IOException e1) {
				System.out.println("Arquivo txt inexistente");
			}
		}
	}


}