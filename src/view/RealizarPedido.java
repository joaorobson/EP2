package view;

import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import controllers.AcoesInterfaceRealizarPedido;

public class RealizarPedido {

	private JPanel painelRealizarPedido;
	
	
	
	public RealizarPedido(JFrame telaInicial,String produtoSelecionado
			,String quantidadeSelecionada,DefaultListModel<String> modeloLista,JComboBox<Integer> quantidade,
			int posicao,JButton inserirPedido,JButton finalizar,boolean bool,int max) throws IOException{
		criaPainelRealizarPedido(telaInicial,produtoSelecionado,
				quantidadeSelecionada,modeloLista,quantidade,posicao,inserirPedido,finalizar, bool,max)	;
		}



	private void criaPainelRealizarPedido(JFrame telaInicial,String produtoSelecionado,
			String quantidadeSelecionada,DefaultListModel<String> modeloLista
			,JComboBox<Integer> quantidade,int posicao,JButton inserirPedido,
			JButton finalizar,boolean bool,int max) throws IOException {
		
		painelRealizarPedido = new JPanel();
		JLabel titulo = new JLabel("Novo pedido");
		JLabel quantidadeTitulo = new JLabel("Quantidade");
		JLabel produtoTitulo = new JLabel("Produto");
		painelRealizarPedido.setLayout(null);
		
		JLabel pedidos = new JLabel("Pedidos");
		JLabel listaProdutos = new JLabel(produtoSelecionado);
		JLabel listaQuantidades = new JLabel(quantidadeSelecionada);
		finalizar = new JButton ("Finalizar pedido");
		
		
		
		pedidos.setBounds(750, 100, 200, 40);
		produtoTitulo.setBounds(350, 145, 100, 30);
		quantidadeTitulo.setBounds(350,185,100,30);
		titulo.setBounds(400,100,200,40);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		pedidos.setFont(new Font("Dialog", Font.PLAIN, 20));
		painelRealizarPedido.add(titulo);
		painelRealizarPedido.add(pedidos);
		
		telaInicial.add(painelRealizarPedido);
		telaInicial.setVisible(true);
		
		String aux=lerProdutos();
		int length;
		String[] e=new String[100];
		String[] alimentos=new String[100];
		alimentos=aux.split(" ");
		length=alimentos.length;
		System.out.println(length);
		int o=0,l=0;		
		String[] alimentos1=new String[length/2];
		if(max<=2){
			while(o<length){
			alimentos1[l]=alimentos[o];
			if(alimentos[o].equals(produtoSelecionado)){
				o=o+2;
				alimentos1[l]=alimentos[o];
			}
			
			o=o+2;
			l++;
		}
			alimentos1[l]="null";
		}
		else{
		while(o<length){
			alimentos1[l]=alimentos[o];
			
			
			o=o+2;
			l++;
		}
		}
		
		//String[] numeros = new String[100];
		
		inserirPedido=new JButton("Inserir pedido na lista");
		inserirPedido.setBounds(510,300,200,30);
				
		JComboBox <String> produto = new JComboBox<String>(alimentos1);
		produto.setSelectedIndex(posicao);
		finalizar.setVisible(bool);
		inserirPedido.setVisible(bool);
		quantidade.setVisible(bool);
		produto.setBounds(450, 150, 100,20);
		produto.setMaximumRowCount(4);
		quantidade.setMaximumRowCount(10);
		produto.setActionCommand("Escolha");
		finalizar.setActionCommand("Finalizar pedido");
		inserirPedido.setActionCommand("Inserir pedido na lista");
		finalizar.addActionListener(new AcoesInterfaceRealizarPedido(painelRealizarPedido, 
				telaInicial, produto,quantidade));
		inserirPedido.addActionListener(new AcoesInterfaceRealizarPedido(painelRealizarPedido, 
				telaInicial, produto,quantidade));
		produto.addActionListener(new AcoesInterfaceRealizarPedido(painelRealizarPedido,
				telaInicial, produto, quantidade));	
		
		
		
		
		finalizar.setBounds(290,300,200,30);
		listaProdutos.setBounds(900,100, 100, 30);
		listaQuantidades.setBounds(850,100,100,20);
		
		
		
		JList lista = new JList(modeloLista);
		lista.setVisibleRowCount(2);
		JScrollPane listScroller = new JScrollPane(lista);
		listScroller.setPreferredSize(new Dimension(20,20));
		listScroller.setBounds(700, 150,200, 100);
		
		painelRealizarPedido.add(listScroller);
		painelRealizarPedido.add(finalizar);
		painelRealizarPedido.add(inserirPedido);
		
		painelRealizarPedido.add(produto);
		painelRealizarPedido.add(quantidadeTitulo);
		painelRealizarPedido.add(produtoTitulo);
		painelRealizarPedido.add(quantidade);
		
		telaInicial.add(painelRealizarPedido);
		telaInicial.setVisible(true);
	}
	private String lerProdutos() throws IOException {
		BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/produtos.txt"));
		String estoque = "";
		
		
		while( leitor.ready() ){
			String produto=leitor.readLine();
			String quantidade=leitor.readLine();
			leitor.readLine();
			estoque = estoque.concat(produto + " " + quantidade + " ");
		}	
		

		
		
		
		
	
		return estoque;
	}
	
}
