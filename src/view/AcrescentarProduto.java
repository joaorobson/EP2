package view;

import java.awt.Font;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import controllers.AcoesInterfaceAcrescentarProduto;

public class AcrescentarProduto {
	
	private JPanel painelAcrescentarProduto;
	
	public AcrescentarProduto(JFrame telaInicial) throws IOException {
	
			criaPainelAcrescentarProduto(telaInicial);
		}

	private void criaPainelAcrescentarProduto(JFrame telaInicial) throws IOException {
		
		painelAcrescentarProduto = new JPanel();
		painelAcrescentarProduto.setLayout(null);
		
		JLabel produto = new JLabel("Insira o nome do produto a ser cadastrado");
		JTextField produtoEntrada = new JTextField();
		JLabel quantidade = new JLabel("Insira a quantidade do produto");
		JTextField quantidadeEntrada = new JTextField();
		JLabel preco = new JLabel("Insira o preço do produto");
		JLabel opcoes = new JLabel("Categoria");
		JTextField precoEntrada = new JTextField();
		JRadioButton comida = new JRadioButton("Prato/Comida",false);
		JRadioButton sobremesa = new JRadioButton("Sobremesa",false);
		JRadioButton bebida = new JRadioButton("Bebida",false);
		ButtonGroup grupo = new ButtonGroup();
	    grupo.add(comida);
	    grupo.add(sobremesa);
	    grupo.add(bebida);
	    
		
		JLabel titulo = new JLabel("Cadastro de produtos");
		JButton ok = new JButton("Ok");
		JButton cancelar = new JButton("Cancelar");
		JButton voltar = new JButton("Voltar");
		
		titulo.setBounds(375,100,250,40);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		opcoes.setBounds(800,100,250,40);
		opcoes.setFont(new Font("Dialog", Font.PLAIN, 20));
		ok.setBounds(345,330,150,30);
		cancelar.setBounds(505,330,150,30);
		voltar.setBounds(345,370, 310, 30);
		
		comida.setBounds(800, 150, 150,20);
		sobremesa.setBounds(800,200,150,20);
		bebida.setBounds(800, 250,150, 20);
		
		
		
		
		produto.setBounds(345,150,335,20);
		produtoEntrada.setBounds(345, 180, 310, 20);
		
		quantidade.setBounds(345,210,335,20);
		quantidadeEntrada.setBounds(345, 240, 310, 20);
		
		preco.setBounds(345,270,335,20);
		precoEntrada.setBounds(345, 300, 310, 20);
		
		
		ok.setActionCommand("Ok");
		cancelar.setActionCommand("Cancelar");
		
		ok.addActionListener( new AcoesInterfaceAcrescentarProduto(painelAcrescentarProduto,telaInicial,
				produtoEntrada,quantidadeEntrada,precoEntrada,comida, bebida, sobremesa));
		cancelar.addActionListener(new AcoesInterfaceAcrescentarProduto(painelAcrescentarProduto,
				telaInicial,produtoEntrada,quantidadeEntrada,precoEntrada, comida, bebida, sobremesa));
		voltar.addActionListener(new AcoesInterfaceAcrescentarProduto(painelAcrescentarProduto,
				telaInicial,produtoEntrada,quantidadeEntrada,precoEntrada,comida,bebida, sobremesa));
			
		
		
		
		
		painelAcrescentarProduto.add(titulo);
		painelAcrescentarProduto.add(ok);
		painelAcrescentarProduto.add(cancelar);
		painelAcrescentarProduto.add(voltar);
		painelAcrescentarProduto.add(produto);
		painelAcrescentarProduto.add(produtoEntrada);
		painelAcrescentarProduto.add(quantidade);
		painelAcrescentarProduto.add(quantidadeEntrada);
		painelAcrescentarProduto.add(preco);
		painelAcrescentarProduto.add(precoEntrada);
		painelAcrescentarProduto.add(comida);
		painelAcrescentarProduto.add(sobremesa);
		painelAcrescentarProduto.add(bebida);
		painelAcrescentarProduto.add(opcoes);
		telaInicial.add(painelAcrescentarProduto);
		telaInicial.setVisible(true);
		painelAcrescentarProduto.setVisible(true);
		
		
		
		
		
	}

	

	
	
}
