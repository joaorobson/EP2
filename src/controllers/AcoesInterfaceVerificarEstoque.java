package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import view.Pedido;

public class AcoesInterfaceVerificarEstoque implements ActionListener {

	
	
	private JFrame telaInicial;
	private JPanel painelVerificarEstoque;
	public AcoesInterfaceVerificarEstoque(JFrame telaInicial,JPanel painelVerificarEstoque){
		this.telaInicial=telaInicial;
		this.painelVerificarEstoque=painelVerificarEstoque;
	}
	
	
	
	public void actionPerformed(ActionEvent e) {
		
		String comando = e.getActionCommand();
		
		if(comando.equals("Voltar")){
			painelVerificarEstoque.setVisible(false);
			new Pedido(telaInicial);
		}
	}

}
