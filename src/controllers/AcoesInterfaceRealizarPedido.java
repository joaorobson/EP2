package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import view.ContaFinal;
import view.RealizarPedido;

public class AcoesInterfaceRealizarPedido implements ActionListener {
	
	private JPanel painelRealizarPedido;
	private JFrame telaInicial;
	private JComboBox<String> produto;	
	private JComboBox<Integer>quantidade;
	private String produtoSelecionado;
	private String quantidadeSelecionada;
	private JButton inserirPedido;
	private JButton finalizar;
	private int quantidadeMax;
	private int quantidadeAtual;
	public AcoesInterfaceRealizarPedido(JPanel painelRealizarPedido, JFrame telaInicial, 
			JComboBox<String> produto,JComboBox<Integer> quantidade) {
		this.painelRealizarPedido=painelRealizarPedido;
		this.telaInicial=telaInicial;
		this.produto=produto;
		this.quantidade=quantidade;
		
		
	}
	static String[] temp=new String[100];
	static String help="0";
	static int i=0;
	public void actionPerformed(ActionEvent e){
			
		String comando = e.getActionCommand();
				
		DefaultListModel<String> modeloLista = new DefaultListModel<String>();

		if(comando.equals("Escolha")){
			
			produtoSelecionado="";
			
			produtoSelecionado=produto.getSelectedItem().toString();
			quantidadeSelecionada="";
			
			try {
				quantidadeMax = comparaProdutos(produtoSelecionado);
			} catch (IOException e2) {
				System.out.println("Arquivo não encontrado!");
			}
			if(quantidadeMax<=2){
				JOptionPane.showMessageDialog(null,"O estoque do produto "+produtoSelecionado+" está "
						+ "abaixo do nível mínimo permititdo!","Aviso",0);
				try {
					quantidadeMax=comparaProdutosFalta(produtoSelecionado);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Integer[] numeros=new Integer[quantidadeMax];
				int w=1;
				while(w<=quantidadeMax){
					numeros[w-1]= w;
					w++;
								
				}
				modeloLista.setSize(100);
				
				temp[i] = "";
				
			
				for(int h=0;h<=i;h++){
					modeloLista.add(h,temp[h].toString());
				}
							
				try {
					painelRealizarPedido.setVisible(false);
					
					new RealizarPedido(telaInicial,produtoSelecionado, quantidadeSelecionada,modeloLista,
							quantidade,posProdutos(produtoSelecionado),inserirPedido,finalizar, true,2);
					
					
					
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
			}
			else  {
			
				try {
					quantidadeMax = comparaProdutos(produtoSelecionado);
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			Integer[] numeros=new Integer[quantidadeMax];
			int w=1;
			while(w<=quantidadeMax){
				numeros[w-1]= w;
				w++;
							
			}
			modeloLista.setSize(100);
			
			temp[i] = "";
			
		
			for(int h=0;h<=i;h++){
				modeloLista.add(h,temp[h].toString());
			}
						
			
			try {
				painelRealizarPedido.setVisible(false);
				
				quantidade = new JComboBox<Integer>(numeros);
				quantidade.setSelectedIndex(0);
				quantidade.setBounds(450,190, 100, 20);
				new RealizarPedido(telaInicial,produtoSelecionado, quantidadeSelecionada,modeloLista,
						quantidade,posProdutos(produtoSelecionado),inserirPedido,finalizar, true,quantidadeMax);
				
				
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
						
			}
		}
		
	

	
		else if(comando.equals("Inserir pedido na lista")){
			
			
			
			
			produtoSelecionado = produto.getSelectedItem().toString();
			try {
				quantidadeMax = comparaProdutos(produtoSelecionado);
			} catch (IOException e2) {
				System.out.println("Arquivo não encontrado!");
			}
			quantidadeSelecionada = quantidade.getSelectedItem().toString();
			help=quantidadeSelecionada;
			
			
			quantidadeAtual=quantidadeMax-Integer.parseInt(quantidadeSelecionada);
			
			if(quantidadeAtual<=2){
				JOptionPane.showMessageDialog(null,"Quantidade de produtos escolhida não permitida! "
						+ "Escolha no máximo "+String.valueOf(quantidadeMax-3)+" produtos!","Aviso",0);
			}
			
				
			
			else{
				
				quantidadeMax=quantidadeMax-Integer.parseInt(quantidadeSelecionada);
				
				if(quantidadeMax<=2){
					JOptionPane.showMessageDialog(null,"O estoque do produto "+produtoSelecionado+" está "
							+ "abaixo do nível mínimo permititdo!","Aviso",0);
					
				}
				
				Integer[] numeros=new Integer[quantidadeAtual];
				int w=1;
				while(w<=quantidadeAtual){
					numeros[w-1]= w;
					w++;
								
				}
				
				modeloLista.setSize(100);
			
				try {
					temp[i] = quantidadeSelecionada +"   " +produtoSelecionado+"   "+ 
				precoProdutos(produtoSelecionado,quantidadeSelecionada)+" ";
				} catch (IOException e2) {
					System.out.println("Arquivo de texto não encontrado!");
				}
		
				
				for(int h=0;h<=i;h++){
					modeloLista.add(h,temp[h].toString());
				}
				i++;
			
	
				painelRealizarPedido.setVisible(false);
				quantidade = new JComboBox<Integer>(numeros);
				quantidade.setSelectedIndex(0);
				quantidade.setBounds(450,190, 100, 20);
				try {
					new RealizarPedido(telaInicial, produtoSelecionado,quantidadeSelecionada,modeloLista,
							quantidade,posProdutos(produtoSelecionado), inserirPedido,finalizar, true,10);
					
				} catch (IOException e1) {
					
				}
			
			}
			

		}
		
else if(comando.equals("Finalizar pedido")){
			
	
			int p = 1; 
			String conta="";
			String q="";
			int z=i;
			String[] r=new String[3*z+1];
			
			
			for(int f = 0;f<i;f++){
				q=q.concat(temp[f].toString())	;
				//System.out.println(temp[f]);
			}
			r=q.split("\\s+");
			
			
			
			
			try {
				baixaEstoque(r);
			} catch (IOException e1) {
				System.out.println("Arquivo de texto não encontrado");
				e1.printStackTrace();
			}
			
			
			
			painelRealizarPedido.setVisible(false);
			new ContaFinal(telaInicial,r);
		}

 
 }

private int comparaProdutos(String a) throws IOException {
	BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/produtos.txt"));
	String produto=leitor.readLine();
	String valor = leitor.readLine();
	
	
	while( leitor.ready() ){
		leitor.readLine();
		if(produto.equals(a)){
			break;
		}
		produto=leitor.readLine();
		valor=leitor.readLine();
	}
	
	int v=Integer.parseInt(valor);
	
	return v;	
	
	
}
private int comparaProdutosFalta(String a)throws IOException{
	BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/produtos.txt"));
	String produto=leitor.readLine();
	String valor=leitor.readLine();
	
	while( leitor.ready() ){
		leitor.readLine();
		if(produto.equals(a)){
			leitor.readLine();
			valor=leitor.readLine();
			break;
		}
		produto=leitor.readLine();
		leitor.readLine();
	}
	
	int v=Integer.parseInt(valor);
	
	return v;	
	
	
	
}



private int posProdutos(String a) throws IOException {
	BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/produtos.txt"));
	
	int p=0;
	
	
	while( leitor.ready() ){
		String produto=leitor.readLine();
		leitor.readLine();
		
		if(produto.equals(a)){
			break;
		}
		p++;
		leitor.readLine();
	
	}
	
	return p;	
	
	
}
private String precoProdutos(String a,String b) throws IOException {
	BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos/produtos.txt"));
	
	String produto=leitor.readLine();
	String preco=leitor.readLine();	
	
	while( leitor.ready() ){
		preco=leitor.readLine();
		if(produto.equals(a)){
			break;
		}
		produto=leitor.readLine();
		leitor.readLine();
		
	
	}
	
	int c = Integer.parseInt(preco);
	int d =Integer.parseInt(b);
	int temp = c*d;
	
	
	return String.valueOf(temp);	
	
	
}


private void baixaEstoque(String[] a) throws IOException {
	
	
	
	BufferedReader leitor = new BufferedReader(new FileReader("/home/joaorobson/Documentos"
			+ "/produtos.txt"));
     String linha;
     String entrada = "";

     while ((linha = leitor.readLine()) != null) entrada += linha + '\n';
    
     String[] entradaNova = new String[entrada.length()];
     entradaNova=entrada.split("\\s+");
     int d=0;
     int h=1;
     int p=0;
    while(p<a.length){
    	 System.out.println(a[p]);
    	 p++;
     }
     
     
     
     
     while(h<a.length){
	     while(d<entradaNova.length){
	    	 System.out.println("a" +a[h]);
	    	 System.out.println("h"+entradaNova[d]);
	    	if(a[h].equals(entradaNova[d])){
	    		String temp11=a[h-1].toString();
	    		String temp21 =entradaNova[d+1].toString();
	    		int novo = Integer.parseInt(temp21)
	    				- Integer.parseInt(temp11);
	    		entrada=entrada.replace(entradaNova[d+1], String.valueOf(novo));
	    		System.out.println(2);
	    		d=0;
	    		break;
	    	}
	    	else{
	    		d=d+3;
	    	}
	    	
	    		
     }
	     h=h+3;
    }
    
    
     FileOutputStream fileOut = new FileOutputStream("/home/joaorobson/Documentos"
 			+ "/produtos.txt");
    fileOut.write(entrada.getBytes());
    fileOut.close();
	
	
	
	
	
	
	
	}
	
		
		
		
		
	}

	





  
	




