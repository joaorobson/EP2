package controllers;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import view.AcrescentarProduto;
import view.InterfaceGraficaInicial;
import view.RealizarPedido;
import view.VerificarEstoque;

public class AcoesInterfacePedido implements ActionListener {
	
	private JPanel painelPedido;
	private JFrame telaInicial;
	
	
	public AcoesInterfacePedido(JPanel painelPedido,JFrame telaInicial){
		this.painelPedido=painelPedido;
		this.telaInicial=telaInicial;
		
	}

	
	public void actionPerformed(ActionEvent e){
			
			
			String comando = e.getActionCommand();
			String q ="";
			String j="";
			int o=0;
			JButton inserirPedido = new JButton();
			JButton finalizar = new JButton();
			JComboBox<Integer> qu=new JComboBox<Integer>();
			DefaultListModel<String> l= new DefaultListModel<String>();
			if(comando.equals("Realizar novo pedido")){
				
				painelPedido.setVisible(false);
				try {
					RealizarPedido pedido = new RealizarPedido(telaInicial,j,q,l,qu,o,inserirPedido,finalizar, false,10);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			   			
			}
			else if(comando.equals("Verificar estoque")){
				painelPedido.setVisible(false);
				
					try {
						VerificarEstoque estoque = new VerificarEstoque(telaInicial);
					} catch (IOException e1) {
						System.out.println("Arquivo de texto não encontrado!");
						e1.printStackTrace();
					}
				
			}
			else if(comando.equals("Acrescentar produto ao estoque")){
				painelPedido.setVisible(false);
				try {
					AcrescentarProduto add = new AcrescentarProduto(telaInicial);
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
				
			}
			else{
				painelPedido.setVisible(false);
				new InterfaceGraficaInicial(telaInicial);
			}
			
		}
	
		
		
	
	
}

