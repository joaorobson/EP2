package view;

import java.awt.Dimension;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ContaFinal {
	JPanel painelContaFinal;
	
	public ContaFinal(JFrame telaInicial,String[] r){
		criaPainelContaFinal(telaInicial, r);
	}
	
	
	
	private void criaPainelContaFinal(JFrame telaInicial, String[] r) {
		
		painelContaFinal = new JPanel();
		painelContaFinal.setLayout(null);
		JLabel conta = new JLabel("Conta final");
		JButton pagarCartao = new JButton("Pagar com cartão de crédito");
		JButton pagarDinheiro = new JButton("Pagar com dinheiro");
		DefaultListModel<String> modelLista = new DefaultListModel<String>();
		modelLista.setSize(30);
		
		for(int i = 0;i<r.length;i++){
			modelLista.add(i,r[i].toString());
			
		}
		conta.setBounds(400,100,100, 30);
		JList lista = new JList(modelLista);
		lista.setVisibleRowCount(3);
		JScrollPane listScroller = new JScrollPane(lista);
		listScroller.setPreferredSize(new Dimension(20,20));
		listScroller.setBounds(300, 200,300, 50);
		pagarCartao.setBounds(200,400,250,30);
		pagarDinheiro.setBounds(500,400,250,30);
		
		painelContaFinal.add(conta);
		painelContaFinal.add(pagarDinheiro);
		painelContaFinal.add(pagarCartao);
		painelContaFinal.add(listScroller);
		telaInicial.add(painelContaFinal);
		telaInicial.setVisible(true);
	}


	
}
