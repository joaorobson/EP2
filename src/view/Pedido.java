package view;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import controllers.AcoesInterfacePedido;


public class Pedido  {

	private JPanel painelPedido;
	
	public Pedido(JFrame telaInicial){
		
		criaPainelPedidos(telaInicial);
	}

		
	
	
	private void criaPainelPedidos(JFrame telaInicial) {
		
		JButton pedido = new JButton("Realizar novo pedido");
		JButton estoque = new JButton("Verificar estoque");
		JButton acrescentarProduto = new JButton("Acrescentar produto ao estoque");
		JLabel titulo = new JLabel("Pedidos");
		JButton sair = new JButton("Sair");
		
		
		painelPedido = new JPanel();
		painelPedido.setLayout(null);
		

		titulo.setBounds(450,100,200,40);
		titulo.setFont(new Font("Dialog", Font.PLAIN, 20));
		pedido.setBounds(350,200,300,30);
		estoque.setBounds(350,250,300,30);
		acrescentarProduto.setBounds(350,300,300,30);
		sair.setBounds(350,370, 300, 30);
		
		
		
		pedido.setActionCommand("Realizar novo pedido");
		estoque.setActionCommand("Verificar estoque");
		acrescentarProduto.setActionCommand("Acrescentar produto ao estoque");
		
		sair.addActionListener(new AcoesInterfacePedido(painelPedido,telaInicial));
		pedido.addActionListener(new AcoesInterfacePedido(painelPedido,telaInicial));
		estoque.addActionListener(new AcoesInterfacePedido(painelPedido,telaInicial));
		acrescentarProduto.addActionListener(new AcoesInterfacePedido(painelPedido,telaInicial));
		painelPedido.add(titulo);
		painelPedido.add(pedido);
		painelPedido.add(estoque);
		painelPedido.add(acrescentarProduto);
		painelPedido.add(sair);
		
		telaInicial.add(painelPedido);
		telaInicial.setVisible(true);
		
		
		
		
		
	}
	
	
	
}
